'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/kategori/:categoryName?', {templateUrl: 'partials/category.html', controller: 'CategoryController'});
  $routeProvider.when('/egitim/:id?', {templateUrl: 'partials/video.html', controller: 'VideoController'});
  $routeProvider.when('/', {templateUrl: 'partials/main.html', controller: 'HomeController'});
  $routeProvider.otherwise({redirectTo: '/'});
}]);
