'use strict';

/* Directives */


angular.module('myApp.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }])
  .directive('itemThumbnail', function(){
    return{
      restrict: 'E',
      templateUrl: 'partials/itemThumbnail.html',
      replace: true
    };
  })
  .directive('carousel', function(){
    return{
      restrict: 'E',
      scope: {
        items: '=',
        carouselTitle: '=carouselTitle'
      },
      templateUrl: 'partials/carousel.html',
      replace: true
    };
  });
