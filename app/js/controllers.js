'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
  .controller('HomeController', ['$scope', function($scope) {
    var carousels = [];
    carousels.push({
      title: "Yeni Egitimler",
      items: [{"id":0,"picture":"http://lorempixel.com/287/215/abstract/6","title":"Eleanor Rivers","description":"id veniam ullamco proident ullamco incididunt nulla reprehenderit pariatur ullamco consequat exercitation veniam ad quis","duration":"15:36","category":"teknoloji"},{"id":2,"picture":"http://lorempixel.com/287/215/nightlife/3","title":"Jacobson Green","description":"tempor minim aute nulla ex ut qui officia et pariatur deserunt laboris ex esse aliqua","duration":"1:23","category":"teknoloji"},{"id":3,"picture":"http://lorempixel.com/287/215/business/10","title":"Gayle Robles","description":"qui eu labore excepteur ut reprehenderit aliqua ex qui pariatur dolor ipsum veniam irure elit","duration":"17:57","category":"teknoloji"}]
    });
    carousels.push({
      title: "Populer Egitimler",
      items: [{"id":4,"picture":"http://lorempixel.com/287/215/city/2","title":"Shepherd Bolton","description":"voluptate tempor pariatur pariatur exercitation commodo deserunt quis duis sunt irure ea duis velit veniam","duration":"23:53","category":"teknoloji"},{"id":5,"picture":"http://lorempixel.com/287/215/cats/10","title":"Sofia Frye","description":"ipsum non nisi excepteur exercitation magna aute dolor qui consequat sunt consequat culpa elit aliqua","duration":"15:32","category":"teknoloji"},{"id":6,"picture":"http://lorempixel.com/287/215/nightlife/5","title":"Cabrera Lott","description":"culpa cillum officia consectetur ea minim deserunt ad magna est consequat qui ea consectetur laborum","duration":"22:46","category":"teknoloji"}]
    });
    $scope.carousels = carousels;


  }])
  .controller('CategoryController', ['$scope', '$routeParams', '$http', function($scope, $routeParams, $http) {
    var url = 'http://localhost:8001/all.json';
    if('categoryName' in $routeParams){
      $scope.categorySelected = true;
      $scope.selectedCategory = $routeParams.categoryName;
      url = url.replace('all', $routeParams.categoryName);
    }

    $http.get(url).success(function(data){
      $scope.items = data;
    });

  }])
  .controller('VideoController', ['$scope', '$routeParams', '$http', function($scope, $routeParams, $http){
    var url = 'http://localhost:8001/egitim/'+$routeParams.id+'.json';
    $http.get(url).success(function(data){
      $scope.item = data;
    });
  }]);
